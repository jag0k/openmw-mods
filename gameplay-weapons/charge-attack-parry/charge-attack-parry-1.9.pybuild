# Copyright 2023 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Charge Attack Parry"
    DESC = "Turn your charge attacks into parries!"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/52184"
    LICENSE = "all-rights-reserved"
    RDEPEND = "base/morrowind"
    NEXUS_SRC_URI = """
        https://www.nexusmods.com/morrowind/mods/52184?tab=files&file_id=1000036223
        -> Solthas_Charge_Attack_Parry-52184-1-9-1677728434.7z
    """
    INSTALL_DIRS = [
        InstallDir(
            "SolChargeAttackParry",
            PLUGINS=[File("SolChargeAttackParry.omwscripts")],
            DOC=["ReadMe SCAP 1.9.md"],
        )
    ]

    def pkg_pretend(self):
        super().pkg_pretend()

        self.warn(
            "This mod only works with OpenMW 0.48 (currently a development build)"
        )
