# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import fnmatch
import json
import os
import re
from typing import (
    Any,
    Callable,
    Dict,
    Generator,
    Iterable,
    List,
    Optional,
    Pattern,
    Set,
    Tuple,
    Union,
    cast,
)

from pybuild import DOWNLOAD_DIR  # noqa
from pybuild import apply_patch  # noqa
from pybuild import Pybuild2, check_required_use, patch_dir, use_reduce


class Package(Pybuild2):
    NAME = "OpenMW"
    DESC = "Pybuild Class for openmw packages"
    KEYWORDS = "openmw tes3mp"


def list_to_re(patterns: List[str]) -> Pattern:
    def to_re(value: Union[str, Pattern]):
        """
        Converts fn-match string into a regular expression string

        Note that normpath may not work as expected with fn-match strings
        if forward-slashes are present inside bracketed ranges (e.g. [/../]).
        """
        if isinstance(value, Pattern):
            return value
        return fnmatch.translate(os.path.normpath(value))

    flags = 0
    if os.environ.get("CASE_INSENSITIVE_FILES", False):
        flags = re.IGNORECASE
    return re.compile("|".join(map(to_re, patterns)), flags=flags)


class File:
    """Represents important installed files and their metadata"""

    def __init__(
        self,
        NAME: str,
        *,
        REQUIRED_USE: str = "",
        OVERRIDES: Union[str, List[str]] = [],
        CLEAN: bool = False,
        TR_PATCH: bool = False,
    ):
        """
        File objects also support a REQUIRED_USE variable, for , and an OVERRIDES variable for overriding other plugins in the load order.
        """
        self.NAME: str = NAME
        """Name of the file relative to the root of the InstallDir"""
        self.REQUIRED_USE: str = REQUIRED_USE
        """
        Requirements for installing this file

        The default empty string is always satisfied.
        See Pybuild2.REQUIRED_USE for details on the syntax.
        """
        self.OVERRIDES: Union[str, List[str]] = OVERRIDES
        """
        A list of files which this overrides when sorting (if applicable).

        Can either be in the form of a string containing use-conditionals (note that
        this does not support files that contain spaces) or a list of files to override.
        Note that these overridden files are not considered masters and do not need to
        be present.

        For archives it determines the order in which the fallback archives will be
        searched during VFS lookups.
        """
        self.CLEAN = CLEAN
        """
        A flag indicating that CleanPlugin from common/utils should clean the file.
        Only meaningful if that class is used in the pybuild.
        """
        self.TR_PATCH = TR_PATCH
        """
        A flag indicating that TRPatcher from common/utils should patch the file.
        Only meaningful if that class is used in the pybuild.
        """

    def dump(self, use: Set[str]) -> Dict[str, Union[List[str], str]]:
        return {
            "name": self.NAME,
            "overrides": use_reduce(self.OVERRIDES, use, flat=True)
            if isinstance(self.OVERRIDES, str)
            else self.OVERRIDES,
        }


class InstallDir:
    """
    Represents a file or directory to be installed
    """

    def __init__(
        self,
        PATH: str,
        *,
        REQUIRED_USE: str = "",
        S: Optional[str] = None,
        WHITELIST: Optional[List[str]] = None,
        BLACKLIST: Optional[Union[List[str], str]] = None,
        RENAME: Optional[str] = None,
        PLUGINS: Iterable[File] = (),
        ARCHIVES: Iterable[File] = (),
        GROUNDCOVER: Iterable[File] = (),
        FILES: Iterable[File] = (),
        ATLASGEN: Iterable[File] = (),
        DOC: Iterable[str] = (),
        PATCHDIR: str = ".",
        DATA_OVERRIDES: str = "",
        CLEAN: bool = False,
    ):
        self.PATH: str = PATH
        """
        The path to the data directory that this InstallDir represents
        relative to the root of the archive it is contained within.
        """
        self.REQUIRED_USE: str = REQUIRED_USE
        """
        A list of use flags with the same format as the package's
        REQUIRED_USE variable which enable the InstallDir if satisfied.
        Defaults to an empty string that is always satisfied.
        """
        self.S: Optional[str] = S
        """
        The source directory corresponding to this InstallDir.

        Similar function to S for the entire pybuild, this determines which directory
        contains this InstallDir, and generally corresponds to the name of the source
        archive, minus extensions. This is required for packages that contain more
        than one source, but is automatically detected for those with only one source
        if it is not specified, and will first take the value of Pybuild2.S, then the
        source's file name without extension if the former was not defined.
        """
        self.WHITELIST: Optional[List[str]] = WHITELIST
        """
        If present, only installs files matching the patterns in this list.
        fnmatch-style globbing patterns (e.g. * and [a-z]) can be used
        """
        self.BLACKLIST: Optional[List[str]] = BLACKLIST
        """
        If present, does not install files matching the patterns in this list.
        fnmatch-style globbing patterns (e.g. * and [a-z]) can be used
        """
        self.RENAME: Optional[str] = RENAME
        """
        Destination path of this directory within the final directory.

        E.g.::

            InstallDir("foo/bar", rename="bar")

        Will install the contents of ``foo/bar`` (in the source) into the directory
        ``bar`` inside the package's installation directory (and also the VFS).
        """
        self.DOC: List[str] = list(DOC)
        """
        A list of patterns matching documentation files within the package

        This documentation will be installed separately
        fnmatch-style globbing patterns (e.g. * and [a-z]) can be used.
        """
        self.PLUGINS = list(PLUGINS)
        """
        List of esp etc. plugins
        """
        self.ARCHIVES = list(ARCHIVES)
        """
        List of bsa archives
        """
        self.GROUNDCOVER = list(GROUNDCOVER)
        """
        List of groundcover plugins
        """
        self.PATCHDIR = PATCHDIR
        """
        The destination path of the InstallDir within the package's directory.

        Defaults to ".", i.e. the root of the mod directory. If multiple InstallDirs
        share the same PATCHDIR they will be installed into the same directory in the
        order that they are defined in the INSTALL_DIRS list.
        Each unique PATCHDIR has its own entry in the VFS, and its own sorting rules
        """
        self.DATA_OVERRIDES: str = DATA_OVERRIDES
        """
        A list of packages that this InstallDir should override in the VFS

        This only has a different effect from MW.DATA_OVERRIDES if multiple PATCHDIRs
        are set, as it can define overrides for individual PATCHDIRS, while
        MW.DATA_OVERRIDES affects all PATCHDIRs.
        See MW.DATA_OVERRIDES for details of the syntax.
        """
        self.FILES = FILES
        """
        Extra miscellaneous files which can be enabled/disabled using REQUIRED_USE
        """
        self.ATLASGEN = ATLASGEN
        """
        Atlas generators for use with common/atlasgen
        """

    def get_files(self) -> List[File]:
        return (
            list(self.PLUGINS)
            + list(self.ARCHIVES)
            + list(self.GROUNDCOVER)
            + list(self.FILES)
            + list(self.ATLASGEN)
        )

    def dump(self, pkg: Pybuild2) -> Dict[str, Any]:
        def iuse_check(flag):
            return flag in pkg.IUSE_EFFECTIVE

        use = pkg.USE

        return {
            "overrides": use_reduce(self.DATA_OVERRIDES, pkg.USE, flat=True),
            "plugins": [
                file.dump(use)
                for file in self.PLUGINS
                if check_required_use(file.REQUIRED_USE, use, iuse_check)
            ],
            "archives": [
                file.dump(use)
                for file in self.ARCHIVES
                if check_required_use(file.REQUIRED_USE, use, iuse_check)
            ],
            "groundcover": [
                file.dump(use)
                for file in self.GROUNDCOVER
                if check_required_use(file.REQUIRED_USE, use, iuse_check)
            ],
        }


class MW(Pybuild2):
    INSTALL_DIRS: List[InstallDir] = []
    """
    The INSTALL_DIRS variable consists of a python list of InstallDir objects.

    E.g.::

        INSTALL_DIRS=[
            InstallDir(
                'Morrowind/Data Files',
                REQUIRED_USE='use use ...',
                DESTPATH='.',
                PLUGINS=[File('Plugin Name',
                    REQUIRED_USE='use use ...', satisfied
                )],
                ARCHIVES=[File('Archive Name')],
                S='Source Name Without Extension',
            )
        ]
    """
    TIER: Union[str, int] = "a"
    """
    The Tier of a package represents the position of its data directories and plugins
    in the virtual file system.

    This is used to group packages in such a way to avoid having to individually
    specify overrides whenever possible.

    The value is either in the range [0-9] or [a-z].

    Default value: 'a'

    Tier 0 represents top-level mods such as morrowind
    Tier 1 is for mods that replace or modify top-level mods. E.g. texture and mesh replacers.
    Tier 2 is for large mods that are designed to be built on top of by other mods, such as Tamriel Data
    Tier a is for all other mods.
    Tier z is for mods that should be installed or loaded last. E.g. omwllf
    The remaining tiers are reserved in case the tier system needs to be expanded
    """
    DATA_OVERRIDES: str = ""
    """
    A use-reduce-able list of atoms indicating packages whose data directories should
    come before the data directories of this package when sorting data directories.

    They do not need to be dependencies. Blockers (atoms beginning with !!) can be used
    to specify underrides, and use dependencies (e.g. the [bar] in foo[bar]) can be
    used to conditionally override based on the target atom's flag configuration.
    """
    REBUILD_FILES: List[str] = []
    """
    A list of files to track for rebuilding with the vfs-rebuild module
    Changes to these files in the VFS will cause the package to be added to the rebuild set
    """

    def __init__(self):
        self.RDEPEND += " >=modules/configtool-0.7"
        self.DEPEND += " modules/vfs-rebuild"

    def get_install_dir_dest(self):
        install_dir_dest = os.environ.get("INSTALL_DEST", ".")
        for attr in dir(self):
            if not attr.startswith("_") and isinstance(getattr(self, attr), str):
                install_dir_dest = install_dir_dest.replace(
                    "{" + attr + "}", getattr(self, attr)
                )
        return os.path.normpath(install_dir_dest)

    def get_install_dir_dest_specific(self, install: InstallDir):
        if install.RENAME is None:
            return os.path.normpath(
                os.path.join(self.get_install_dir_dest(), install.PATCHDIR)
            )
        else:
            return os.path.normpath(
                os.path.join(
                    self.get_install_dir_dest(),
                    install.PATCHDIR,
                    install.RENAME,
                )
            )

    def get_files(self, typ: str) -> Generator[Tuple[InstallDir, File], None, None]:
        for install_dir in self.INSTALL_DIRS:
            if check_required_use(
                install_dir.REQUIRED_USE, self.get_use(), self.valid_use
            ):
                for file in getattr(install_dir, typ, ()):
                    if check_required_use(
                        file.REQUIRED_USE, self.get_use(), self.valid_use
                    ):
                        yield install_dir, file

    def install(self, source: str, install: InstallDir, to_install: Set[str]):
        case_insensitive = os.environ.get("CASE_INSENSITIVE_FILES", False)

        dest = os.path.join(self.D, self.get_install_dir_dest_specific(install))
        # FIXME: Need to filter things in PLUGINS, ARCHIVES, GROUNDCOVER, FILES, etc.
        # when their REQUIRED_USE is not satisfied

        filter_entries = install.BLACKLIST or []
        if isinstance(filter_entries, str):
            filter_entries = [filter_entries]

        for file in install.get_files():
            # ignore files which will not be used
            if not check_required_use(
                file.REQUIRED_USE, self.get_use(), self.valid_use
            ):
                filter_entries.append(file.NAME)

        filter_re = list_to_re(filter_entries)
        if install.WHITELIST is None:
            include_only = None
        else:
            include_only = list_to_re(install.WHITELIST)

        def get_listfn(filter_re: Pattern, polarity: bool):
            def fn(directory: str, contents: Iterable[str]):
                paths = []
                basedir = os.path.relpath(directory, source)
                for file in contents:
                    path = os.path.normpath(os.path.join(basedir, file))
                    paths.append(path)

                if polarity:
                    return {
                        file
                        for path, file in zip(paths, contents)
                        if filter_re.match(path)
                        and not os.path.isdir(os.path.join(directory, file))
                    }
                else:
                    return {
                        file
                        for path, file in zip(paths, contents)
                        if not filter_re.match(path)
                        and not os.path.isdir(os.path.join(directory, file))
                    }

            return fn

        # Function to ingore additional paths, given an existing ignore function
        def ignore_more(
            ignorefn, to_ignore: List[str]
        ) -> Callable[[str, List[str]], Set[str]]:
            def fn(directory: str, contents: Iterable[str]):
                results = ignorefn(directory, contents)
                for name in contents:
                    if any(
                        os.path.normpath(os.path.join(directory, name))
                        == os.path.normpath(path)
                        for path in to_ignore
                    ):
                        results.add(name)
                return results

            return fn

        # Determine if any other InstallDirs are inside this one and add them to the blacklist
        to_ignore = []
        for other_path in to_install:
            if other_path != source and os.path.commonpath(
                [source]
            ) == os.path.commonpath(
                [os.path.abspath(source), os.path.abspath(other_path)]
            ):
                to_ignore.append(other_path)

        os.makedirs(os.path.dirname(dest), exist_ok=True)

        if os.path.islink(source):
            linkto = os.readlink(source)
            if os.path.exists(dest):
                os.rmdir(dest)
            os.symlink(linkto, dest, True)
        elif os.path.isfile(source):
            os.rename(source, dest)
        elif include_only is not None:
            ignore = get_listfn(include_only, False)
            if to_ignore:
                ignore = ignore_more(ignore, to_ignore)
            patch_dir(source, dest, ignore=ignore, case_sensitive=not case_insensitive)
        elif filter_entries:
            ignore = get_listfn(filter_re, True)
            if to_ignore:
                ignore = ignore_more(ignore, to_ignore)
            patch_dir(source, dest, ignore=ignore, case_sensitive=not case_insensitive)
        else:
            ignore = None
            if to_ignore:
                ignore = ignore_more(lambda d, c: set(), to_ignore)
            patch_dir(source, dest, case_sensitive=not case_insensitive, ignore=ignore)

    def get_metadata(self):
        return {
            "id": self.CP,
            "path": os.path.normpath(
                os.path.join(self.ROOT, self.get_install_dir_dest())
            ),
            "plugins": [],
            "archives": [],
            "groundcover": [],
            "tier": str(self.TIER),
            "overrides": use_reduce(self.DATA_OVERRIDES, self.USE, flat=True),
            "flags": self.USE,
            "fallback": self.FALLBACK if hasattr(self, "FALLBACK") else None,
            "settings": self.SETTINGS if hasattr(self, "SETTINGS") else None,
        }

    def write_metadata(self, metadata: Dict):
        meta_path = os.path.join(
            self.D, os.environ["CONFIGPATH"], "configtool", metadata["id"] + ".json"
        )
        os.makedirs(os.path.dirname(meta_path), exist_ok=True)
        with open(meta_path, "w") as file:
            json.dump(metadata, file)

    def unpack(self, archives):
        for archive in archives:
            if isinstance(archive, str):
                archive_path = archive
                archive_name = os.path.basename(archive)
            else:
                archive_name, ext = os.path.splitext(archive.name)
                if archive_name.lower().endswith(".tar"):
                    archive_name, second_ext = os.path.splitext(archive_name)
                    ext = (second_ext + ext).lower()
                archive_path = archive.path

            if ext in (".zip", ".tar", ".tar.bz2", ".tar.xz", ".tar.gz"):
                super().unpack([archive])
            else:
                self.execute(["7z", "x", "-y", "-o" + archive_name, archive_path])

    def src_install(self):
        to_install: List[Tuple[str, InstallDir, bool]] = []
        sources = set()
        for install in self.INSTALL_DIRS:
            source_dir = install.S or cast(str, self.S)
            source = os.path.normpath(
                os.path.join(self.WORKDIR, source_dir, install.PATH)
            )
            sources.add(source)
            if check_required_use(install.REQUIRED_USE, self.USE, self.valid_use):
                # self.S will be set in package.py via the PhaseState
                to_install.append((source, install, True))
            else:
                to_install.append((source, install, False))

        metadata = {".": self.get_metadata()}

        for source, install, enabled in to_install:
            source_dir_path = os.path.join(install.S or cast(str, self.S), install.PATH)
            if enabled:
                dest = self.get_install_dir_dest_specific(install)
                print(
                    # FIXME: repo-side localization
                    # l10n(
                    #    "installing-directory-into",
                    #    dir=magenta(source_dir_path),
                    #    dest=magenta(os.path.join(self.ROOT, install.RENAME)),
                    # )
                    f"Installing {source_dir_path} into {os.path.join(self.D, dest)}"
                )
                for doc_path in set(install.DOC) | {
                    "README*",
                    "ChangeLog",
                    "CHANGELOG*",
                    "AUTHORS*",
                    "NEWS*",
                    "TODO*",
                    "CHANGES*",
                    "THANKS*",
                    "BUGS*",
                    "FAQ*",
                    "CREDITS*",
                    "Doc/*",
                    "doc/*",
                    "docs/*",
                    "Docs/*",
                }:
                    self.dodoc(os.path.join(self.WORKDIR, source_dir_path, doc_path))
                self.install(source, install, sources)
                if install.PATCHDIR not in metadata:
                    # Create a separate metadata file just for this patchdir
                    metadata[install.PATCHDIR] = self.get_metadata()
                    metadata[install.PATCHDIR]["id"] = (
                        self.CPN + "-" + install.PATCHDIR + "-" + self.PV
                    )
                    metadata[install.PATCHDIR]["path"] = os.path.normpath(
                        os.path.join(
                            self.ROOT, self.get_install_dir_dest(), install.PATCHDIR
                        )
                    )

                # Merge with existing
                install_data = install.dump(self)
                metadata[install.PATCHDIR]["plugins"].extend(install_data["plugins"])
                metadata[install.PATCHDIR]["archives"].extend(install_data["archives"])
                metadata[install.PATCHDIR]["groundcover"].extend(
                    install_data["groundcover"]
                )
                metadata[install.PATCHDIR]["overrides"].extend(
                    install_data["overrides"]
                )
            else:
                print(
                    # FIXME: repo-side localization
                    # l10n(
                    #    "skipping-directory",
                    #    dir=magenta(source_dir_path),
                    #    req=blue(install.REQUIRED_USE),
                    # )
                    f"Skipping {source_dir_path} due to unsatisfied use requirements {install.REQUIRED_USE}"
                )

        for entry in metadata.values():
            self.write_metadata(entry)

        if self.REBUILD_FILES:
            from vfs_rebuild import create_rebuild_manifest

            create_rebuild_manifest(self.PN, self.REBUILD_FILES, root=self.D)
