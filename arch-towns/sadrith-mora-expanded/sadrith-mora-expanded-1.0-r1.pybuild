# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, File, InstallDir
from common.nexus import NexusMod
from common.util import CLEAN_DEPEND, CleanPlugin

EPIC_FILE = "Epic_Sadrith_Mora_Expanded-44113-1-0"


class Package(CleanPlugin, NexusMod, MW):
    NAME = "Sadrith Mora Expanded"
    DESC = "Expands the city of Sadrith Mora, such as an upper district behind Tel Naga"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/44113"
    LICENSE = "all-rights-reserved"
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        tr? (
            landmasses/tamriel-data
            landmasses/tamriel-rebuilt
        )
        epic-sadrith-mora? (
            arch-towns/epic-sadrith-mora
        )
    """
    MANUAL_CLEAN_DEPEND = True
    DEPEND = f"""
        base/morrowind
        tr? (
            landmasses/tamriel-data
            landmasses/tamriel-rebuilt
            {CLEAN_DEPEND}
        )
    """
    KEYWORDS = "openmw"
    SRC_URI = f"""
        Sadrith_Mora_Expanded-44113-1-0.rar
        {EPIC_FILE}.rar
    """
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/44113"
    IUSE = "tr epic-sadrith-mora"

    INSTALL_DIRS = [
        InstallDir(
            ".",
            PLUGINS=[
                File("sadrith mora expanded TR.esp", REQUIRED_USE="tr", CLEAN=True),
                File("sadrith mora expanded.esp"),
            ],
            S="Sadrith_Mora_Expanded-44113-1-0",
            REQUIRED_USE="!epic-sadrith-mora",
        ),
        InstallDir(
            ".",
            PLUGINS=[
                File("Sadrith Mora Expanded-EpicComp.esp"),
                File(
                    "Sadrith Mora Expanded-EpicCompTR.esp",
                    REQUIRED_USE="tr",
                    CLEAN=True,
                ),
            ],
            S="Epic_Sadrith_Mora_Expanded-44113-1-0",
            REQUIRED_USE="epic-sadrith-mora",
        ),
    ]
