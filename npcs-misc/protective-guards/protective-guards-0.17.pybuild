# Copyright 2023 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Protective Guards"
    DESC = "Makes guards protect against hostile NPCs"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/46992"
    LICENSE = "all-rights-reserved"
    RDEPEND = "base/morrowind[bloodmoon,tribunal]"
    NEXUS_SRC_URI = """
        https://www.nexusmods.com/morrowind/mods/46992?tab=files&file_id=1000033076
        -> Protective_Guards_for_OpenMW_-_Lua_Edition-46992-0-17-1663095163.7z
    """
    INSTALL_DIRS = [
        InstallDir(
            "Protective Guards for OpenMW - Lua Edition",
            PLUGINS=[
                File("Protective Guards for OpenMW - Lua Edition.ESP"),
                File("protective_guards_for_omw.omwscripts"),
            ],
            DOC=["readme pls!.txt"],
        )
    ]

    def pkg_pretend(self):
        super().pkg_pretend()

        self.warn(
            "This mod only works with OpenMW 0.48 (currently a development build)"
        )
