# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Graphic Herbalism"
    DESC = "Automatically harvests herbs using the container's leveled lists"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/46599"
    # Note: contains a patched copy of correctUV Diverse Ore Veins,
    # which permits use and redistribution of derivatives
    LICENSE = "attribution"
    # Trama root patch was incorporated into Patch for purists 3.1.0
    # Project atlas 0.7 changed the atlases which this uses, making it incompatible
    # You should only use the atlas generators with later versions of project atlas
    # (it also has a patch for graphic herbalism, though that only provides the vanilla
    # meshes, they won't be the glowing-bitter-coast or smooth versions
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        pfp? ( >=base/patch-for-purists-3.1.0 )
        tr? ( landmasses/tamriel-rebuilt )
        atlas? ( <assets-misc/project-atlas-0.7 )
        glowing-bitter-coast? ( glowing-bitter-coast )

        assets-misc/hackle-lo-fixed
        assets-misc/pherim-fire-fern
        assets-misc/cornberry-replacer
    """
    # Note: Pherim's replacers are included as dependencies since it would be
    # unnecessarily complicated to have use aliases and conditional dependencies for
    # all of them.
    # Open a feature request if this is an issue for you.
    KEYWORDS = "openmw"
    SRC_URI = """
        Graphic_Herbalism_MWSE_-_OpenMW-46599-1-04-1558643353.7z
        GH_Patches_and_Replacers-46599-1-02-1558643538.7z
        tr? ( GH_TR_-_PT_Meshes-46599-1-03-1558643754.7z )
        !pfp? ( GH_trama_patch-46599-1-0-1556731265.7z )
    """
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/46599"
    TEXTURE_SIZES = "!smooth? ( 512 ) smooth? ( 1024 )"

    # Should override the following mods as noted in readme
    DATA_OVERRIDES = """
        base/morrowind-rebirth
        atlas? ( assets-misc/project-atlas )
        glowing-bitter-coast? ( assets-misc/glowing-bitter-coast )
        tr? ( landmasses/tamriel-rebuilt )
        assets-misc/hackle-lo-fixed
        assets-misc/pherim-fire-fern
        assets-misc/cornberry-replacer
    """

    IUSE = """
        atlas
        glowing-bitter-coast
        smooth
        ore-respawn
        less-epic-plants
        slightly-less-epic-plants
        tr
        +pfp
    """

    INSTALL_DIRS = [
        InstallDir(
            "00 Core + Vanilla Meshes",
            S="Graphic_Herbalism_MWSE_-_OpenMW-46599-1-04-1558643353",
        ),
        InstallDir(
            "01 Optional - Smoothed Meshes",
            S="Graphic_Herbalism_MWSE_-_OpenMW-46599-1-04-1558643353",
            REQUIRED_USE="smooth",
        ),
        InstallDir(
            "00 Correct UV Ore + README",
            PLUGINS=[
                File(
                    "correctUV Ore Replacer_respawning.esp", REQUIRED_USE="ore-respawn"
                ),
                File("correctUV Ore Replacer_fixed.esp", REQUIRED_USE="!ore-respawn"),
            ],
            S="GH_Patches_and_Replacers-46599-1-02-1558643538",
        ),
        # Requires:
        # cornberry: https://www.nexusmods.com/morrowind/mods/42586
        # fire fern: https://www.nexusmods.com/morrowind/mods/43568
        # hackle-lo: https://www.nexusmods.com/morrowind/mods/42784
        InstallDir(
            "01 Pherim's Replacers",
            S="GH_Patches_and_Replacers-46599-1-02-1558643538",
        ),
        # Reflection maps should not be used with OpenMW according to readme
        # InstallDir(
        #    "02 Pherim Reflection Mapped",
        #    S="GH_Patches_and_Replacers-46599-1-02-1558643538",
        # ),
        # TODO: Requires: https://www.nexusmods.com/morrowind/mods/43555
        # InstallDir(
        #    "03 Pherim Pulsing Kwama",
        #    S="GH_Patches_and_Replacers-46599-1-02-1558643538",
        # ),
        # Reflection maps should not be used with OpenMW according to readme
        # InstallDir(
        #    "04 Pherim Pulsing Kwama Reflect",
        #    S="GH_Patches_and_Replacers-46599-1-02-1558643538",
        # ),
        # Texture Size: 1448
        # TODO: Requires Vurt's leafy west gash II
        # InstallDir(
        #    "05 Vurt's Chokeweed & Roobrush",
        #    S="GH_Patches_and_Replacers-46599-1-02-1558643538",
        # ),
        InstallDir(
            "06 Less Epic Plants",
            S="GH_Patches_and_Replacers-46599-1-02-1558643538",
            REQUIRED_USE="|| ( less-epic-plants slightly-less-epic-plants )",
        ),
        InstallDir(
            "07 Slightly Less Epic Plants",
            S="GH_Patches_and_Replacers-46599-1-02-1558643538",
            REQUIRED_USE="slightly-less-epic-plants",
        ),
        InstallDir(
            "08 Glowing Bitter Coast",
            S="GH_Patches_and_Replacers-46599-1-02-1558643538",
            REQUIRED_USE="glowing-bitter-coast !smooth",
        ),
        InstallDir(
            "09 Glowing Bitter Coast Smoothed",
            S="GH_Patches_and_Replacers-46599-1-02-1558643538",
            REQUIRED_USE="glowing-bitter-coast smooth",
        ),
        InstallDir(
            "10 Atlas - Vanilla BC Mushrooms",
            S="GH_Patches_and_Replacers-46599-1-02-1558643538",
            REQUIRED_USE="atlas",
        ),
        InstallDir(
            "11 Atlas - Glowing Bitter Coast Patch",
            S="GH_Patches_and_Replacers-46599-1-02-1558643538",
            REQUIRED_USE="atlas glowing-bitter-coast",
        ),
        InstallDir(
            "12 Atlas - Smoothed BC Mushrooms",
            S="GH_Patches_and_Replacers-46599-1-02-1558643538",
            REQUIRED_USE="atlas smooth",
        ),
        InstallDir(
            "13 Atlas - Smoothed Glowmap Patch",
            S="GH_Patches_and_Replacers-46599-1-02-1558643538",
            REQUIRED_USE="atlas smooth glowing-bitter-coast",
        ),
        # TODO: Requires Remiros shelf fungus mod
        # InstallDir(
        #    "14 Remiros shelf fungus",
        #    S="GH_Patches_and_Replacers-46599-1-02-1558643538",
        # ),
        # TODO: Requires Apel's Asura Coast and Sheogorath Retexture
        # InstallDir(
        #    "15 Apel's Azura's Coast",
        #    S="GH_Patches_and_Replacers-46599-1-02-1558643538",
        # ),
        # Reflection maps should not be used with OpenMW according to readme
        # InstallDir(
        #    "16 Apel's Mucksponge Bumpmapped",
        #    S="GH_Patches_and_Replacers-46599-1-02-1558643538",
        # ),
        # InstallDir(
        #    "17 Trama Bumpmapped",
        #    S="GH_Patches_and_Replacers-46599-1-02-1558643538",
        # ),
        # InstallDir(
        #    "18 Ascadian Isles Plants",
        #    S="GH_Patches_and_Replacers-46599-1-02-1558643538",
        # ),
        # TODO: Requires Glass Glowset
        # InstallDir(
        #    "19 Glass Glowset ores",
        #    S="GH_Patches_and_Replacers-46599-1-02-1558643538",
        # ),
        # TODO: Requires Morrowind Crafting or ST Alchemy
        # InstallDir(
        #     "20 Vanilla Meshes for MC or STA",
        #     S="GH_Patches_and_Replacers-46599-1-02-1558643538",
        # ),
        # TODO: Requires Morrowind Crafting
        # InstallDir(
        #     "21 Cave Plant Replacer for MC",
        #     S="GH_Patches_and_Replacers-46599-1-02-1558643538",
        # ),
        # TODO: Requires Morrowind Crafting or ST Alchemy
        # InstallDir(
        #     "22 Kelp Replacer for MC or STA",
        #     S="GH_Patches_and_Replacers-46599-1-02-1558643538",
        # ),
        InstallDir(
            ".",
            PLUGINS=[File("GH_TestCell_TR.esp")],
            S="GH_TR_-_PT_Meshes-46599-1-03-1558643754",
            REQUIRED_USE="tr",
        ),
        InstallDir(
            ".",
            PLUGINS=[File("GH_trama_patch.esp")],
            S="GH_trama_patch-46599-1-0-1556731265",
            REQUIRED_USE="!pfp",
        ),
    ]
